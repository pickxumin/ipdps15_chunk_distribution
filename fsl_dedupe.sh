#!/bin/bash

trace="/home/ldfs/data/fslhomes"
wdir="/home/ldfs/min/chunk-dev"
#users=('000' '001' '002' '003' '004' '005' '006' '007' '008' '010' '011' '012' '013' '014' '015' '016' '017' '018' '019' '020' '021' '022' '023' '024' '025' '026' '027' '028')
users=('028' '004' '015' '008' '013' '022' '026' '007' '012')
years=(2013)

cd $wdir
for year in ${years[@]}; do
	for month in {1..12}; do 
		for date in {1..31}; do
			for user in ${users[@]}; do
			snapshot="fslhomes-user${user}-${year}-$(echo $month | awk '{printf "%.2d", $1}')-$(echo $date | awk '{printf "%.2d", $1}')"
			if [ -f $trace/$year/$snapshot.tar.gz ]; then
				sleep 1
				if [ ! -f $wdir/$snapshot/$snapshot.4kb.hash.anon ]; then
				   	tar zxf $trace/$year/$snapshot.tar.gz
					echo "./fsl_analyzer $wdir/$snapshot/$snapshot.4kb.hash.anon"
				   	echo $snapshot >>  $wdir/log/fsl_dedup_${user}_$year.log
				   	./fsl_analyzer $wdir/$snapshot/$snapshot.4kb.hash.anon $user $month-$date 2>> $wdir/log/fsl_dedup_${user}_$year.log
				   	rm -rf $snapshot
				else
					echo $snapshot >> $wdir/log/fsl_dedup_${user}_$year.log
					./fsl_analyzer $wdir/$snapshot/$snapshot.4kb.hash.anon $user $month-$date 2>> $wdir/log/fsl_dedup_${user}_$year.log
				fi
			fi
			done
			rm -rf $wdir/meta/fslindex_$month-$date
		done
	done
done
