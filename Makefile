CC=gcc
CFLAGS= -g -Wall -O0 -I$(IDIR)
LIB=-lpthread -lm -lkyotocabinet -lcrypto -lhashfile -lgf_complete
IDIR=include
LDIR=lib

SIMOBJ=server.c dedupe.c listener.o image.o bloom.o distributor.o segment.o EDP/dedup.o EDP/parity_declustering.o
CLIOBJ=client.o chunking.o rabin.o fingerprint.o rpc/llist_xdr.o rpc/llist_clnt.o galois.o jerasure.o reed_sol.o
SLVOBJ=slave.o
all: server client slave read_hashfile sim_downloader 

.SUFFIXES: .c .o
.c.o:
	$(CC) $(CFLAGS) -c $*.c

reed_sol.h: lib/jerasure/reed_sol.h
	cp lib/jerasure/reed_sol.h . ; chmod 0444 reed_sol.h
 
reed_sol.c: lib/jerasure/reed_sol.c
	rm -f reed_sol.c ; cp lib/jerasure/reed_sol.c . ; chmod 0444 reed_sol.c

jerasure.h: lib/jerasure/jerasure.h
	rm -f jerasure.h ; cp lib/jerasure/jerasure.h . ; chmod 0444 jerasure.h
 
jerasure.c: lib/jerasure/jerasure.c
	rm -f jerasure.c ; cp lib/jerasure/jerasure.c . ; chmod 0444 jerasure.c

galois.h: lib/jerasure/galois.h
	rm -f galois.h ; cp lib/jerasure/galois.h . ; chmod 0444 galois.h

galois.c: lib/jerasure/galois.c
	rm -f galois.c ; cp lib/jerasure/galois.c . ; chmod 0444 galois.c

galois.o: galois.h
jerasure.o: jerasure.h galois.h

server: $(SIMOBJ)
	cd EDP; $(MAKE)
	$(CC) -L$(LDIR) -o server $^ $(LIB) $(CFLAGS)

client: $(CLIOBJ)
	cd rpc; $(MAKE)
	$(CC) $(CFLAGS) -L$(LDIR) -o client $^ $(LIB)

slave: $(SLVOBJ)
	$(CC) $(CFLAGS) -L$(LDIR) -o slave $^ $(LIB)

read_hashfile: read_hashfile.c
	$(CC) -Wall read_hashfile.c -L$(LDIR) -lhashfile -o read_hashfile $(LIB)

sim_downloader: sim_downloader.c
	$(CC) $(CFLAGS) sim_downloader.c -L$(LDIR) -o sim_downloader $(LIB)

4430_analyzer_intra: vm_analytic.c
	$(CC) $(CFLAGS) vm_analytic.c -L$(LDIR) -o 4430_analyzer_intra $(LIB)

clean:
	rm -rf server client slave read_hashfile *.o
	cd EDP && $(MAKE) clean
	cd rpc && $(MAKE) clean 
