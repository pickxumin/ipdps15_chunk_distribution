# Chunk Distribution over Reliable Storage Cluster with Deduplication #

### How do I get set up? ###

#### Dependency ####
* rpcbind
#### How to Setup ####
  * `./init.sh`
  * `make`
#### How to Run ####
  * **Start Slaves**
    * `./prepare.sh`
  * **Start Server**
    * `cd rpc; sudo ./rpc_svc.sh; cd ..`
    * `./server LOCAL_PORT BASELINE|EDP`
  * **Upload**
    * `./client SERV_IP SERV_PORT FILE_PATH FILE_PATH F|D`
  * **Download**
    * `./client SERV_IP INST_NUM/256 INST_NUM VERS_NUM`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact