#!/bin/bash

trace="/home/ldfs/data/linux_distro"
wdir="/home/ldfs/min/chunk-dev"
vers=('v2.6.35' 'v3.0.20' 'v3.0.57' 'v3.0.93' 'v3.1.2' 'v3.2.12' 'v3.2.49' 'v3.4.27' 'v3.4.63' 'v3.6.3' 'v3.8.3' 'v3.10.13' 'v3.13.5' 'v3.15' 'zstable')
host=127.0.0.1
port=12345
nodes=16
schemes=('BASELINE' 'EDP')

cd $wdir
for s in ${schemes[@]}; do

./prepare.sh
./server $port $s 2>> serv.log&

for ver in ${vers[@]}; do
			snapshot="linux-$ver"
			./client $host $port $trace/$snapshot $trace/$snapshot D | grep 'Node' >> $wdir/log/linux_write_$s
			sleep 2
			ins=`find image -type f | wc -l`
			./sim_downloader $ins $wdir/log/linux_read_$s $snapshot
			rm -rf $wdir/meta/flog $wdir/meta/ilog $wdir/image/*
done

pkill server
rm -rf serv.log
sleep 60
 
done
