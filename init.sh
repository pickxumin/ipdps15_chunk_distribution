#!/bin/bash

#Install Packages
#1. GF_COMPLETE
cd lib/jerasure
tar zxf gf-complete.tar.gz
cd gf-complete
sudo make install

mkdir image meta log
for i in {0..11}; do 
    mkdir node$i
    dd if=/dev/zero of=node${i}/chunk_store bs=1G count=1
done
