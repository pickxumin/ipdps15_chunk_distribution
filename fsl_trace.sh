#!/bin/bash

trace="/home/ldfs/data/fslhomes/2013"
wdir="/home/ldfs/min/chunk-dev"
users=('028' '004' '015' '008' '013' '022' '026' '007' '012')
year=2013
host=127.0.0.1
port=12346
nodes=(10 11 12 13 14)
datas=(8 9 10 11 12)
bsize=(2160 3465 5280 7722 10920)
schemes=('BASELINE' 'EDP')
cnt=7

cd $wdir
#for j in {2..4}; do
	
#	sed -i "s/\#define ARRAY_SIZE/\#define ARRAY_SIZE ${nodes[$j]} \/\//g" common.h
#	sed -i "s/\#define STRIPE_SIZE/\#define STRIPE_SIZE ${datas[$j]} \/\//g" common.h
#	sed -i "s/\#define SEG_BUF_SIZE/\#define SEG_BUF_SIZE ${bsize[$j]} \/\//g" common.h
#	sed -i "s/HEHE/\/\/HEHE/g" sim_downloader.c

#	make clean
#    make

#for s in ${schemes[@]}; do

#./prepare.sh
#./server $port $s 2>> serv.log &

for month in {1..6}; do 
	for date in {1..31}; do
		cnt=$(echo "$cnt+1"|bc)
		for user in ${users[@]}; do
			snapshot="fslhomes-user${user}-${year}-0${month}-$(echo $date | awk '{printf "%.2d", $1}')"
			if [ -f $trace/$snapshot.tar.gz ]; then
			if [ $cnt -gt 7 ]; then
			tar zxf $trace/${snapshot}.tar.gz
			./client $host $port $wdir/$snapshot/$snapshot.4kb.hash.anon $wdir/$snapshot/$snapshot.4kb.hash.anon F | grep 'Node' >> $wdir/log/fsl_write_${nodes[$j]}_${datas[$j]}_$s
			#sleep 2
			#ins=`find image -type f | wc -l`
			#./sim_downloader $ins $wdir/log/fsl_read_${nodes[$j]}_${datas[$j]}_$s $snapshot
			rm -rf $wdir/meta/flog $wdir/meta/ilog $wdir/image/*
			rm -rf $snapshot
			fi
			fi
		done
		if [ $cnt -gt 7 ]; then
			cnt=0
		fi
	done
done

pkill server
rm -rf serv.log
sleep 60

#done
#done
