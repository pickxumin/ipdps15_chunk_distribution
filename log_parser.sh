#!/bin/bash

#Overall Average
echo "Overall Average: `cat $1 | grep Ratio | awk '{if ($2 > 0) {s+=$2}; if ($2 ~ /[0-9.]/) {n+=1}}END{print s/n}'`"


#Overall Average Excluding Zero
echo "Overall Average Exclusing Zero: `cat $1 | grep Ratio| awk '{ if ($2 != 0 ) {s+=$2;n+=1}}END{print s/n}'`"

#Average for one specific day
echo "Average for $2-$3: `cat $1 | grep Ratio | awk '{if (NR % 4) {printf("%s ",$0)} else {printf("%s\n",$0)}}' | grep "$2-$3" | awk '{if ($15 != "") {s+=$15;n+=1}}END{print s/n}'`"

#Histogram of Improvement Ratio
num=`cat $1 | wc -l`
echo "-1--0.9: `cat $1 | awk '{if ($1 < -0.9 && $1 >= -1 ) n+=1}END{print n}'`/$num"
echo "-0.9--0.8: `cat $1 | awk '{if ($1 < -0.8 && $1 >= -0.9 )n+=1}END{print n}'`/$num"
echo "-0.8-0.7: `cat $1 | awk '{if ($1 < -0.7 && $1 >= -0.8 )n+=1}END{print n}'`/$num"
echo "-0.7-0.6: `cat $1 | awk '{if ($1 < -0.6 && $1 >= -0.7 )n+=1}END{print n}'`/$num"
echo "-0.6-0.5: `cat $1 | awk '{if ($1 < -0.5 && $1 >= -0.6 )n+=1}END{print n}'`/$num"
echo "-0.5-0.4: `cat $1 | awk '{if ($1 < -0.4 && $1 >= -0.5 )n+=1}END{print n}'`/$num"
echo "-0.4-0.3: `cat $1 | awk '{if ($1 < -0.3 && $1 >= -0.4 )n+=1}END{print n}'`/$num"
echo "-0.3-0.2: `cat $1 | awk '{if ($1 < -0.2 && $1 >= -0.3 )n+=1}END{print n}'`/$num"
echo "-0.2-0.1: `cat $1 | awk '{if ($1 < -0.1 && $1 >= -0.2 )n+=1}END{print n}'`/$num"
echo "-0.1-0: `cat $1 | awk '{if ($1 < 0 && $1 >= -0.1 )n+=1}END{print n}'`/$num"
echo "0-0.1: `cat $1 | awk '{if ($1 > 0 && $1 <= 0.1 ) n+=1}END{print n}'`/$num"
echo "0.1-0.2: `cat $1 | awk '{if ($1 > 0.1 && $1 <= 0.2 )n+=1}END{print n}'`/$num"
echo "0.2-0.3: `cat $1 | awk '{if ($1 > 0.2 && $1 <= 0.3 )n+=1}END{print n}'`/$num"
echo "0.3-0.4: `cat $1 | awk '{if ($1 > 0.3 && $1 <= 0.4 )n+=1}END{print n}'`/$num"
echo "0.4-0.5: `cat $1 | awk '{if ($1 > 0.4 && $1 <= 0.5 )n+=1}END{print n}'`/$num"
echo "0.5-0.6: `cat $1 | awk '{if ($1 > 0.5 && $1 <= 0.6 )n+=1}END{print n}'`/$num"
echo "0.6-0.7: `cat $1 | awk '{if ($1 > 0.6 && $1 <= 0.7 )n+=1}END{print n}'`/$num"
echo "0.7-0.8: `cat $1 | awk '{if ($1 > 0.7 && $1 <= 0.8 )n+=1}END{print n}'`/$num"
echo "0.8-0.9: `cat $1 | awk '{if ($1 > 0.8 && $1 <= 0.9 )n+=1}END{print n}'`/$num"
echo "0.9-1: `cat $1 | awk '{if ($1 > 0.9 && $1 <= 1 )n+=1}END{print n}'`/$num"
