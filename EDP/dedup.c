#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "dedup.h"
#include "parity_declustering.h"
/*
 * 
 * NextPlacement 3
 * 
 */
int NextPlacement3(int n, int* duplicate_array, int* U, int* storage_management, int* cost_array){
	int i,j,p;
	int retval;
	
	int index = -1;
	double read_cost = 10000.0;
	p = -1;
	
	for(i=n-1; i>=0; --i){
		// candidate, disk i
		double possible_read_cost = 0.0;
		for(j=0; j<n; ++j){
			double tmp = 0.0;
			if(j == i){
				tmp = (double)(duplicate_array[j]+U[j]+1)/cost_array[j];
			}
			else{
				tmp = (double)(duplicate_array[j]+U[j])/cost_array[j];
			}
			if(tmp >= possible_read_cost){
				possible_read_cost = tmp;
			}
		}
		
		if(possible_read_cost < read_cost){
			read_cost = possible_read_cost;
			index = i;
			p = storage_management[i];
		}
		else if(possible_read_cost == read_cost){
			if(storage_management[i] <= p){
				index = i;
				p = storage_management[i];
			}
		}
		
	}
	
	retval = index;
	return retval;
}

int CostNextPlacement(int r, int n, int* Ui, int* Di, int* F, int* cost_array){
	int i,j,p;
	int index = -1;
	double read_cost = 10000000.0;
	p = -1;
	for(i=n-1; i>=0; --i){
		// candidate, disk i
		double possible_read_cost = 0.0;
		for(j=0; j<n; ++j){
			if(F[j]+1 <= r-parityNumInDisk){
				double tmp = 0.0;
				if(j == i){
					tmp = (double)(Di[j]+Ui[j]+1)/cost_array[j];
				}else{
					tmp = (double)(Di[j]+Ui[j])/cost_array[j];
				}
				if(tmp >= possible_read_cost){
					  possible_read_cost = tmp;
				}
			}
		}
		if(F[i]+1 <= r-parityNumInDisk){
			if(possible_read_cost < read_cost){
				read_cost = possible_read_cost;
				index = i;
				p = F[i];
			}
			else if(possible_read_cost == read_cost){
				if(F[i] <= p){
					index = i;
					p = F[i];
				}
			}
		}
	}
	return index;
}


double CostComputeWSDIndex(int r, int n, int numberOfExistingFiles, int start, int* U, int* D, int* cost_array){
	int num = numberOfExistingFiles;
	int i,j;
	int* Di = (int*)malloc(sizeof(int)*n);
	int* Ui = (int*)malloc(sizeof(int)*n);
	double segmentIndex = 0.0;
	double fileIndex = 0.0;
	double segmentImpr = 0.0;
	double max = 0.0;
	double sum = 0.0;
	//int totalnid = 0;
	//int totalniu = 0;
	//fprintf(stderr, " ****** compute segment index ****** \n");
	for(i=start; i<num; ++i){
		// the i-th file
		for(j=0; j<n; ++j){			
			Di[j] = D[i*n + j];
			Ui[j] = U[i*n + j];
			sum += ((double)D[i*n+j]+U[i*n+j])/(double)cost_array[j];
			if (((double)D[i*n+j]+U[i*n+j])/(double)cost_array[j] > max) max = ((double)D[i*n+j]+U[i*n+j])/(double)cost_array[j];
		}
		// 1) file index
		double denominator = 0.0;
		double numerator = 0.0;
		for(j=0; j<n; ++j){
			denominator += (double)(Di[j]+Ui[j])/cost_array[j];
		}
		denominator = denominator*denominator;
		for(j=0; j<n; ++j){
			numerator += (double)(Di[j]+Ui[j])*(Di[j]+Ui[j])/(cost_array[j]*cost_array[j]);
		}
		numerator = numerator*(double)n;
		fileIndex = denominator/numerator;
		//fprintf(stderr, "file %d index: %lf.\n", i, fileIndex);
		// 2) segment index
		segmentIndex += fileIndex;
		segmentImpr += (1-n*max/sum);
	}
	//fprintf(stderr, "segment index; %lf.\n", segmentIndex);
	//fprintf(stderr, " *********************************** \n");
	//return segmentIndex;
	return segmentImpr;
}

/*
 * sub-function 1
 */
int NextPlacement(int r, int n, int* Ui, int* Di, int* F){
	int i,j;
	int num;
	j = n-1;
	num = Ui[j] + Di[j];
	while(F[j]+1 > r-parityNumInDisk){
		j--;
		num = Ui[j] + Di[j];
	}
	int freeOffset;
	freeOffset = F[j];
	for(i=j-1; i>=0; --i){
		if(Ui[i] + Di[i] <= num && F[i]+1 <= r-parityNumInDisk){
			if(Ui[i] + Di[i] == num){
				if(F[i] <= freeOffset){
					num = Ui[i] + Di[i];
					freeOffset = F[i];
					j = i;
				}
				else{
				}
			}
			else{
				num = Ui[i] + Di[i];
				freeOffset = F[i];
				j = i;
			}
		}
	}

	return j;
}

/*
 * sub-function 2
 */
double ComputeWSDIndex(int r, int n, int numberOfExistingFiles, int start, int* U, int* D){
	int num = numberOfExistingFiles;
	int i,j;
	//int* Di = (int*)malloc(sizeof(int)*n);
	//int* Ui = (int*)malloc(sizeof(int)*n);
	double segmentIndex = 0.0;
	double segmentImpr = 0.0;
	double fileIndex = 0.0;
	int fileBottleneck = 0;
	long long fileNum = 0;
	int totalnid = 0;
	int totalniu = 0;
	//fprintf(stderr, " ****** compute segment index ****** \n");
	for(i=start; i<num; ++i){
		// the i-th file
		fileNum = 0;
		for(j=0; j<n; ++j){			
			//Di[j] = D[i*n + j];
			//Ui[j] = U[i*n + j];
			//Take MAX{Uij+Dij} as the min-max fairness index
			if (D[i*n+j]+U[i*n+j] > fileBottleneck) fileBottleneck = D[i*n+j]+U[i*n+j];

			fileNum += D[i*n+j]+U[i*n+j];
		}
		//fprintf(stderr,"Bottleneck of file %d: %d\n",i,fileBottleneck);
		// 1) file index
		int denominator = 0;
		int numerator = 0;
		int nid = 0;
		int niu = 0;
		for(j=0; j<n; ++j){
			denominator += D[i*n+j]+U[i*n+j];
			nid += D[i*n+j];
			niu += U[i*n+j];
		}
		denominator = denominator*denominator;
		for(j=0; j<n; ++j){
			numerator += (D[i*n+j]+U[i*n+j])*(D[i*n+j]+U[i*n+j]);
		}
		numerator = numerator*n;
		fileIndex = (double)denominator/(double)numerator;
		//fprintf(stderr, "file %d index: %lf.\n", i, fileIndex);
		// 2) segment index
		segmentIndex += fileIndex;//*((double)(nid+niu));
		segmentImpr += ((double)fileNum/(double)n-fileBottleneck)/((double)fileNum/(double)n);
		totalnid += nid;
		totalniu += niu;
	}
	segmentIndex = segmentIndex/((double)(totalnid + totalniu));
	//fprintf(stderr,"Current segment improvement %lf\n",segmentImpr);
	//free(Di);
	//free(Ui);
	//fprintf(stderr, "segment index; %lf.\n", segmentIndex);
	//fprintf(stderr, " *********************************** \n");
	//return segmentIndex;
	return segmentImpr;
}

// sub-sub-function 3.1
void exchangeAllocation(int n, int* U, int* GU, int i, int j, int* Ui, int* Uj, int* GUi, int* GUj, int t, int l, int s){
	GUi[t] -= s;
	GUi[l] += s;
	GUj[t] += s;
	GUj[l] -= s;
	Ui[t] -= s;
	Ui[l] += s;
	Uj[t] += s;
	Uj[l] -= s;
	int q;
	for(q=0; q<n; ++q){
		U[i*n + q] = Ui[q];
		GU[i*n + q] = GUi[q];
		U[j*n + q] = Uj[q];
		GU[j*n + q] = GUj[q];
	}
	
}

// sub-sub-function 3.2
void resumeAllocation(int n, int* U, int* GU, int i, int j, int* Ui, int* Uj, int* GUi, int* GUj, int t, int l, int s){
	GUi[t] += s;
	GUi[l] -= s;
	GUj[t] -= s;
	GUj[l] += s;
	Ui[t] += s;
	Ui[l] -= s;
	Uj[t] -= s;
	Uj[l] += s;
	int q;
	for(q=0; q<n; ++q){
		U[i*n + q] = Ui[q];
		GU[i*n + q] = GUi[q];
		U[j*n + q] = Uj[q];
		GU[j*n + q] = GUj[q];
	}
}

static int segment_counter=0;

/*
 * sub-function 3
 */
int adjustPlacement(int r, int n, int* D, int* U, int* GU, int i, double SI){
	int j, q, s;
	int t, l;
	int x=-1;
	double baseSI = SI;
	double tmpSI = 0.0;
	int* Ui = (int*)malloc(sizeof(int)*n);
	int* Uj = (int*)malloc(sizeof(int)*n);
	int* GUi = (int*)malloc(sizeof(int)*n);
	int* GUj = (int*)malloc(sizeof(int)*n);
	for(q=0; q<n; ++q){
		Ui[q] = U[i*n + q];
		GUi[q] = GU[i*n + q];
	}
	for(j=0; j<i; ++j){
		// each file j prior to the current file i
		for(q=0; q<n; ++q){
			Uj[q] = U[j*n + q];
			GUj[q] = GU[j*n + q];
		}
		for(t=0; t<n; ++t){
			// disk for file i
			for(l=t+1; l<n; ++l){
				// disk for file j
				// consider every possible exchange amount x
				int tmp_t = GUi[t];
				int tmp_l = GUj[l];
				int min = (tmp_t <= tmp_l ? tmp_t : tmp_l);
				for(s=1; s<=min; ++s){
					exchangeAllocation(n, U, GU, i, j, Ui, Uj, GUi, GUj, t, l, s);
					tmpSI = ComputeWSDIndex(r, n, i+1,0,U, D);
					if(tmpSI > baseSI){
						baseSI = tmpSI;
						x = s;
					}
					resumeAllocation(n, U, GU, i, j, Ui, Uj, GUi, GUj, t, l, s);
				}
				if(x!=-1)
				  exchangeAllocation(n, U, GU, i, j, Ui, Uj, GUi, GUj, t, l, x);
				x=-1;
			}
		}
	}
	//fprintf(stderr, "after adjustment, segment index increases to %lf ~ ~ ~ ~ ~ ~\n", baseSI);
	free(Ui);
	free(Uj);
	free(GUi);
	free(GUj);
	return 1;
}

/*
 * sub-function 3
 */
int CostAdjustPlacement(int r, int n, int* D, int* U, int* GU, int i, double SI, int* cost_array){
	int j, q, s;
	int t, l;
	int x=-1;
	double baseSI = SI;
	double tmpSI = 0.0;
	int* Ui = (int*)malloc(sizeof(int)*n);
	int* Uj = (int*)malloc(sizeof(int)*n);
	int* GUi = (int*)malloc(sizeof(int)*n);
	int* GUj = (int*)malloc(sizeof(int)*n);
	for(q=0; q<n; ++q){
		Ui[q] = U[i*n + q];
		GUi[q] = GU[i*n + q];
	}
	for(j=0; j<i; ++j){
		// each file j prior to the current file i
		for(q=0; q<n; ++q){
			Uj[q] = U[j*n + q];
			GUj[q] = GU[j*n + q];
		}
		for(t=0; t<n; ++t){
			// disk for file i
			for(l=t+1; l<n; ++l){
				// disk for file j
				// consider every possible exchange amount x
				int tmp_t = GUi[t];
				int tmp_l = GUj[l];
				int min = (tmp_t <= tmp_l ? tmp_t : tmp_l);
				for(s=1; s<=min; ++s){
					exchangeAllocation(n, U, GU, i, j, Ui, Uj, GUi, GUj, t, l, s);
					tmpSI = CostComputeWSDIndex(r, n, i+1,0, U, D, cost_array);
					if(tmpSI > baseSI){
						baseSI = tmpSI;
						x = s;
					}
					resumeAllocation(n, U, GU, i, j, Ui, Uj, GUi, GUj, t, l, s);
				}
				if(x!=-1)
				  exchangeAllocation(n, U, GU, i, j, Ui, Uj, GUi, GUj, t, l, x);
				x=-1;
			}
		}
	}
	//fprintf(stderr, "after adjustment, segment index increases to %lf ~ ~ ~ ~ ~ ~\n", baseSI);
	free(Ui);
	free(Uj);
	free(GUi);
	free(GUj);
	return 1;
}

/* 
 * Even Data Placement
 * @param r: number of rows in one segment 
 * @param n: number of disks
 * @param f: number of unique blocks for each file
 * @param fSize: number of files in one segment
 * @param DArray: elements i*n...(i+1)*n records the storage allocation for duplicate blocks in file i
 * @param RArray: records all duplicate blocks and its referred file
 * @param sizeofR: size of RArray
 * @param k: parameter of the adopted erasure code, the number of data
 * @param m: parameter of the adopted erasure code, the number of parity 
 * @return
 *       how unique blocks are placed in the segment
 */
struct PlacementInfor* EDP(int r, int n, int* f, int fSize, int* DArray, int* PDArray,struct ReferIntraSegment* RArray, int sizeofR, int k, int m){
	//fprintf(stderr, "EDP, Balanced Data Placement. %d files\n",fSize);
	struct timeval a,b,c;
	fullcompleteblockdesigntable(k, m, n);
	
	int i,j,q,s,t;
	int* F = (int*)malloc(sizeof(int)*n); // F stores the free offset for each storage disk
	for(i=0; i<n; ++i){
		F[i] = 0;
	}
	int* pstat = (int*)malloc(sizeof(int)*n*n*fSize); //number of parities on i-th node for j-th column 													 					   //and k-th file: pstat[k*n*n+j*n+i]
	memset(pstat,0,sizeof(int)*n*n*fSize);
	/*
	for (t=0;t<fSize;t++)
		for (i=0;i<n;i++)
			for (j=0;j<n;j++)
				pstat[t*n*n+i*n+j] = PDArray[t*n*n+i*n+j];
	*/
	
	int* Ui = (int*)malloc(sizeof(int)*n);
	int* GUi = (int*)malloc(sizeof(int)*n);
	int* LUi = (int*)malloc(sizeof(int)*n);
	int* Di = (int*)malloc(sizeof(int)*n);
	
	// compute the number of local unique blocks for each file
	// according to "struct InnerDup* R"
	int* lu = (int*)malloc(sizeof(int)*fSize);
	for(i=0; i<fSize; ++i){
		lu[i] = 0;
	}
	for(i=0; i<sizeofR; ++i){
		int fileID;
		int maxIndex = 0;
		for(j=0; j<fSize; ++j){
			maxIndex += f[j];
			if(RArray[i].block_id < maxIndex){
				fileID = j;
				break;
			}
		}
		lu[fileID]++;
	}
	/*
	fprintf(stderr, "lu, records the number of local(global) unique blocks in each file:<lunique(gunique)>\n");
	for(i=0; i<fSize; ++i){
	    fprintf(stderr, "%d(%d), ", lu[i], f[i]-lu[i]);
	}
	fprintf(stderr, "\n");
	fprintf(stderr, "RArray, records the reference information:<blk_id(f_id)>\n");
	*/
	int lbid = -1;
	int cnt6 = 0;
	for(i=0; i<sizeofR; i++){
		if (RArray[i].file_id == 6 && lbid != RArray[i].block_id){
	    	//fprintf(stderr,"%d(%d), ", RArray[i].block_id, RArray[i].file_id);
			lbid = RArray[i].block_id;
			cnt6++;
		}
	}
	//fprintf(stderr,"%d local duplicates\n",cnt6);
	//fprintf(stderr, "DArray, records the number of duplicate blocks in each file:<f_id(duplicates)>\n");
	// TODO
	int* blockdiskid = (int*)malloc(sizeof(int)*dataNumInSegment);
	int* blockdiskoffset = (int*)malloc(sizeof(int)*dataNumInSegment);
	struct PlacementInfor* edp_solution = (struct PlacementInfor*) malloc (sizeof(struct PlacementInfor)*dataNumInSegment);
	for(i=0; i<dataNumInSegment; ++i){
		blockdiskid[i] = blockdiskoffset[i] = 0;
	}

	for(i=0; i<fSize; ++i){
		//int tempc=0;
		//fprintf(stderr,"File %d: ",i);
		for(j=0; j<n; j++){
		  //tempc=tempc+DArray[i*n+j];
		  //fprintf(stderr, "%d ", DArray[i*n+j]);
		}
		//fprintf(stderr, "\n");	
	}
	// U, GU, LU
	int* U = (int*)malloc(sizeof(int)*fSize*n);
	int* LU = (int*)malloc(sizeof(int)*fSize*n);
	int* GU = (int*)malloc(sizeof(int)*fSize*n);
	for(i=0; i<fSize*n; ++i){
		U[i] = LU[i] = GU[i] = 0;
	}
	
	int Index4LU;
	int Index4U;
	int* ranchor = (int*)malloc(sizeof(int)*n);
	gettimeofday(&a,NULL);
	for(i=0; i<fSize; ++i){
		//int fileID = i;
		Index4LU = 0;
		Index4U = 0;
		for(j=0; j<n; ++j){
			Ui[j] = 0;
			GUi[j] = 0;
			LUi[j] = 0;
		}
		for(j=0; j<n; ++j){
			Di[j] = DArray[i*n + j];
		}
		
		for(j=0; j<i; ++j){
			int* LUjtmp = (int*)malloc(sizeof(int)*n);
			for(q=0; q<n; ++q){
				LUjtmp[q] = LU[j*n + q];
			}
			for(q=Index4LU; q<Index4LU+lu[j]; q++){
				if(RArray[q].file_id == i){
					int LUBlockIDInFileJ = q-Index4LU;
					//int LUBlockDiskID;
					s = 0;
					for(t=0; t<n; ++t){
						s += LUjtmp[t];
						if(LUBlockIDInFileJ < s){
							//LUBlockDiskID = t;
							break;
						}
					}
					//Di[LUBlockDiskID]++;
				}
			}
			free(LUjtmp);
			Index4LU += lu[j];
			Index4U += f[j];
		}
		for(j=0; j<n; ++j){
			//DArray[i*n + j] = Di[j];
			Di[j] = DArray[i*n+j];
		}
		
		int tmpfi = f[i];
		int block_id = Index4U;
		memset(ranchor,0,sizeof(int)*n);
		// first step, place each unique block
		while(tmpfi > 0){
			int tmpDiskID;
			tmpDiskID = NextPlacement(r, n, Ui, Di, F);
			F[tmpDiskID]++;
			Ui[tmpDiskID]++;
			int tmp = 0;
			int lfid = -1;
			for(q=Index4LU; q<Index4LU+lu[i]; q++){
				if(RArray[q].block_id == block_id){
					tmp = 1;
					if (lfid != RArray[q].file_id) {
						DArray[RArray[q].file_id*n+tmpDiskID]++;
						lfid = RArray[q].file_id;
					}
					//break;
				}
			}
			if(tmp == 0){
				GUi[tmpDiskID]++;
			} else {
				LUi[tmpDiskID]++;
			}
			int row,p_cnt;
			 blockdiskid[block_id] = tmpDiskID;
			for (row=ranchor[tmpDiskID];row<(numInSegment/n);row++) {
				if (bitMap[row*n+tmpDiskID] == 0 && groupIden[row*n+tmpDiskID] > 0) {
					ranchor[tmpDiskID] = row+1;
					blockdiskoffset[block_id] = row;
					edp_solution[block_id].gid = groupIden[row*n+tmpDiskID]-1;
					//fprintf(stderr,"No.%d: GID: %d\n",block_id, edp_solution[block_id].gid);
					p_cnt = 0;
					for (j=0;p_cnt<m;j++){
						if (groupIden[anchors[groupIden[row*n+tmpDiskID]-1]+j] == -(groupIden[row*n+tmpDiskID])) {
							if (p_cnt == 0)
								bitMap[row*n+tmpDiskID] = 1;
							edp_solution[block_id].parities[p_cnt] = (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n;
							if (edp_solution[block_id].parities[p_cnt] >= n)
								fprintf(stderr,"Block %d: parity %d to disk %d\n",block_id,p_cnt,edp_solution[block_id].parities[p_cnt]);
							p_cnt++;
						}
					}
					break;
				}
			}
	 		tmpfi--;
			block_id++;
		}
		int total = 0;
		for(j=0; j<n; ++j){
			U[i*n + j] = Ui[j];
			total += (Ui[j]+Di[j]);
			LU[i*n + j] = LUi[j];
			GU[i*n + j] = GUi[j];
		}		
	
		
		double SI = ComputeWSDIndex(r, n, i+1,0,U, DArray); // the weighted segment distribution index
		double fSI = ComputeWSDIndex(r, n, i+1,i,U, DArray);
		//fprintf(stderr, "file index: %lf,%lf* * * * * * * * * * * * * * * * *\n", fSI,-(double)(n)/total);
		//if (fSI < -(double)(n)/total)
		adjustPlacement(r, n, DArray, U, GU, i, SI);

	}
	gettimeofday(&b,NULL);
	timersub(&b,&a,&c);
	fprintf(stderr,"EDP: %ld.%06ld\n",c.tv_sec,c.tv_usec);
	free(ranchor);
	/*
	for (t=0;t<fSize;t++){
		fprintf(stderr,"File %d: ",t);
		for (i=0;i<n;i++){
			//fprintf(stderr,"For node %d: ",i);
			//for (j=0;j<n;j++)
			fprintf(stderr,"%d, ",U[t*n+i]);
		}
		fprintf(stderr,"\n");
	}
	*/

	Index4U = 0;
	int* newF = (int*)malloc(sizeof(int)*n);
	for(i=0; i<n; ++i){
		newF[i] = 0;
	}

	
	
	// TODO
	/*
	int* pmap = (int*)malloc(sizeof(int)*n);
	*/
	/*
	gettimeofday(&a,NULL);
	for(i=0; i<fSize; ++i){
		for(j=0; j<n; ++j){
			Ui[j] = U[i*n + j];
		}
		int block_id = Index4U;
		int tmpDiskID;
		//for(block_id=Index4U; block_id<Index4U + f[i];){
			for(t=0; t<n; ++t){
				tmpDiskID = t;
				for(s=0; s<Ui[t]; ++s){				
					blockdiskid[block_id] = tmpDiskID;
			
					//Search the row where the offset lies
					//int row,p_cnt,p_index;
					//int pnode, psmallest, prow, pfound = 0;
					//memset(pmap,0,sizeof(int)*n);
					//Determine the node k with smallest p_{i,j,k}
					int row,p_cnt;
					for (row=ranchor[tmpDiskID];row<(numInSegment/n);row++) {
						if (bitMap[row*n+tmpDiskID] == 0 && groupIden[row*n+tmpDiskID] > 0) {
							ranchor[tmpDiskID] = row+1;
							blockdiskoffset[block_id] = row;
							edp_solution[block_id].gid = groupIden[row*n+tmpDiskID]-1;
							//fprintf(stderr,"No.%d: GID: %d\n",block_id, edp_solution[block_id].gid);
							p_cnt = 0;
							for (j=0;p_cnt<m;j++){
								if (groupIden[anchors[groupIden[row*n+tmpDiskID]-1]+j] == -(groupIden[row*n+tmpDiskID])) {
									if (p_cnt == 0)
										bitMap[row*n+tmpDiskID] = 1;
									edp_solution[block_id].parities[p_cnt] = (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n;
									if (edp_solution[block_id].parities[p_cnt] >= n)
										fprintf(stderr,"Block %d: parity %d to disk %d\n",block_id,p_cnt,edp_solution[block_id].parities[p_cnt]);
									p_cnt++;
								}
							}
							break;
						}
					}

					
					while(pfound == 0){
						psmallest = 999999;
					for (j=0;j<n;j++){
						if (pstat[i*n*n+tmpDiskID*n+j] < psmallest && pmap[j] == 0 && j != tmpDiskID) {
							pnode = j;
							psmallest = pstat[i*n*n+tmpDiskID*n+j];
						}
						//fprintf(stderr,"%d(%d), ",pstat[i*n*n+tmpDiskID*n+j],pmap[j]);
					}
					//fprintf(stderr,"\n");
					pmap[pnode] = 1;
					//fprintf(stderr,"smallest on node %d(%d,%d)\n",pnode,pstat[i*n*n+tmpDiskID*n+pnode],psmallest);
					for (prow=panchor[pnode];prow<(numInSegment/n);prow++) {
						// Find the available parity 
						if (groupIden[prow*n+pnode] < 0) {
							for (row=(anchors[-groupIden[prow*n+pnode]-1])/n;row<(numInSegment/n);row++) {
								if (bitMap[row*n+tmpDiskID] == 0 && groupIden[row*n+tmpDiskID] == -groupIden[prow*n+pnode]) {
									pfound = 1;
									panchor[pnode] = prow+1;
									blockdiskoffset[block_id] = row;
									edp_solution[block_id].gid = groupIden[row*n+tmpDiskID]-1;
									pstat[i*n*n+tmpDiskID*n+pnode]++;
									bitMap[row*n+tmpDiskID] = 1;
									edp_solution[block_id].parities[0] = pnode;
									//bitMap[prow*n+pnode] = 1;
									p_cnt = 1;
									for (j=0;p_cnt<m;j++){
										if (groupIden[anchors[groupIden[row*n+tmpDiskID]-1]+j] == -(groupIden[row*n+tmpDiskID]) && (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n != pnode) {
											edp_solution[block_id].parities[p_cnt] = (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n;
											//bitMap[anchors[groupIden[row*n+tmpDiskID]-1]+j] = 1;
											p_cnt++;
										}
									}
									break;
								}
							}
						}
						if (pfound == 1)
							break;
					}
					}
					

					for (row=0;row<(numInSegment/n);row++) {
						if (bitMap[row*n+tmpDiskID] == 0 && groupIden[row*n+tmpDiskID] > 0) {
							int smallest=1;  // indicates whether this row guarantees even parities. 1: yes; 0: no
							blockdiskoffset[block_id] = row;
							edp_solution[block_id].gid = groupIden[row*n+tmpDiskID]-1;
							p_cnt = 0;
							p_index = 0;
							for (j=0;p_cnt<m;j++){
								if (groupIden[anchors[groupIden[row*n+tmpDiskID]-1]+j] == -(groupIden[row*n+tmpDiskID])) {
									smallest = 1;
									for (k=0;k<n;k++){ 
										if (k != tmpDiskID){
											if (pstat[i*n*n+tmpDiskID*n+(anchors[groupIden[row*n+tmpDiskID]-1]+j)%n] > pstat[i*n*n+tmpDiskID*n+k]){
												smallest = 0;
												break;
											}
										}	
									}
									if (smallest == 1){
										p_index = p_cnt;
										break;
									}
									p_cnt++;
								}
							}	
							if (smallest == 1) {
								p_cnt = 0;
								for (j=0;p_cnt<m;j++){
									if (groupIden[anchors[groupIden[row*n+tmpDiskID]-1]+j] == -(groupIden[row*n+tmpDiskID])) {
										if (p_cnt == p_index) {
											pstat[i*n*n+tmpDiskID*n+(anchors[groupIden[row*n+tmpDiskID]-1]+j)%n]++;
											bitMap[row*n+tmpDiskID] = 1;
											edp_solution[block_id].parities[0] = (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n;
										} else if (p_cnt == 0) {
											edp_solution[block_id].parities[p_index] = (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n;
										} else {
											edp_solution[block_id].parities[p_cnt] = (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n;
										}
										bitMap[anchors[groupIden[row*n+tmpDiskID]-1]+j] = 1;
										p_cnt++;
									}
								}
								break;
							}
						}
					}
					if (row == (numInSegment/n)) {
						p_cnt = 0;
						for (j=0;p_cnt<m;j++){
							if (groupIden[anchors[groupIden[blockdiskoffset[block_id]*n+tmpDiskID]-1]+j] == -(groupIden[blockdiskoffset[block_id]*n+tmpDiskID])) {
								if (p_cnt == 0){
									pstat[i*n*n+tmpDiskID*n+(anchors[groupIden[blockdiskoffset[block_id]*n+tmpDiskID]-1]+j)%n]++;
									bitMap[blockdiskoffset[block_id]*n+tmpDiskID] = 1;
								}	
								edp_solution[block_id].parities[p_cnt] = (anchors[groupIden[blockdiskoffset[block_id]*n+tmpDiskID]-1]+j)%n;
								p_cnt++;
							}
						}			
					}
					block_id++;
				}
			}	
		//}
		Index4U += f[i];
	}
	gettimeofday(&b,NULL);
	timersub(&b,&a,&c);
	fprintf(stderr,"PackResult: %ld.%06ld\n",c.tv_sec,c.tv_usec);
	*/

	//free(pmap);
	//free(ranchor);
	free(blockIden);
	free(groupIden);
	free(bitMap);
	
	//fprintf(stderr, "file access time:\n");
	/*
	FILE* fp = fopen("edp_fat.txt", "a+");	
	for(i=0; i<fSize; ++i){
		for(j=0; j<n; ++j){
			Ui[j] = U[i*n + j];
			Di[j] = DArray[i*n + j];
		}
		int maxNumInSingleDisk = -1;
		for(j=0; j<n; ++j){
			if(Ui[j] + Di[j] > maxNumInSingleDisk){
				maxNumInSingleDisk = Ui[j] + Di[j];
			}
		}
		//fprintf(stderr, "file %d, access time %d.\n", i, maxNumInSingleDisk);
		fprintf(fp, "%d %d\n", i, maxNumInSingleDisk);
	}
	fprintf(fp, "%d\n", -1);
	fclose(fp);	
	*/

	//the final block placement
	for(i=0; i<dataNumInSegment; ++i){
		edp_solution[i].node_id = blockdiskid[i];
		edp_solution[i].offset = blockdiskoffset[i];
	}
	free(F);
	free(pstat);
	free(Ui);
	free(GUi);
	free(LUi);
	free(Di);
	free(lu);
	free(U);
	free(GU);
	free(LU);
	free(newF);
	free(blockdiskid);
	free(blockdiskoffset);
	segment_counter++;
	//fprintf(stderr, "===========================SEGMENT %d=============================\n\n", segment_counter);		
	return edp_solution;
}

struct PlacementInfor* CEDP(int r, int n, int* f, int fSize, int* DArray, struct ReferIntraSegment* RArray, int* cost_array, int sizeofR, int k, int m){

	fprintf(stderr, "CEDP, Cost-based Balanced Data Placement.\n");
	fullcompleteblockdesigntable(k, m, n);

	int i,j,q,s,t;
	int* F = (int*)malloc(sizeof(int)*n); // F stores the free offset for each storage disk
	for(i=0; i<n; ++i){
		F[i] = 0;
	}
	
	int* Ui = (int*)malloc(sizeof(int)*n);
	int* GUi = (int*)malloc(sizeof(int)*n);
	int* LUi = (int*)malloc(sizeof(int)*n);
	int* Di = (int*)malloc(sizeof(int)*n);
	
	// compute the number of local unique blocks for each file
	// according to "struct InnerDup* R"
	int* lu = (int*)malloc(sizeof(int)*fSize);
	for(i=0; i<fSize; ++i){
		lu[i] = 0;
	}
	for(i=0; i<sizeofR; ++i){
		int fileID;
		int maxIndex = 0;
		for(j=0; j<fSize; ++j){
			maxIndex += f[j];
			if(RArray[i].block_id < maxIndex){
				fileID = j;
				break;
			}
		}
		lu[fileID]++;
	}
	/*
	fprintf(stderr, "lu, records the number of local(global) unique blocks in each file:<lunique(gunique)>\n");
	for(i=0; i<fSize; ++i){
	    fprintf(stderr, "%d(%d), ", lu[i], f[i]-lu[i]);
	}
	fprintf(stderr, "\n");
	fprintf(stderr, "RArray, records the reference information:<blk_id(f_id)>\n");
	for(i=0; i<sizeofR; i++){
	    fprintf(stderr,"%d(%d), ", RArray[i].block_id, RArray[i].file_id);
	}
	fprintf(stderr,"\n");
	fprintf(stderr, "DArray, records the number of duplicate blocks in each file:<f_id(duplicates)>\n");
	for(i=0; i<fSize; ++i){
		//int tempc=0;
		fprintf(stderr,"File %d: ",i);
		for(j=0; j<n; j++){
		  //tempc=tempc+DArray[i*n+j];
		  fprintf(stderr, "%d ", DArray[i*n+j]);
		}
		fprintf(stderr,"\n");	
	}
	*/
	// U, GU, LU
	int* U = (int*)malloc(sizeof(int)*fSize*n);
	int* LU = (int*)malloc(sizeof(int)*fSize*n);
	int* GU = (int*)malloc(sizeof(int)*fSize*n);
	for(i=0; i<fSize*n; ++i){
		U[i] = LU[i] = GU[i] = 0;
	}
	int* blockdiskid = (int*)malloc(sizeof(int)*dataNumInSegment);
	int* blockdiskoffset = (int*)malloc(sizeof(int)*dataNumInSegment);
	struct PlacementInfor* edp_solution = (struct PlacementInfor*) malloc (sizeof(struct PlacementInfor)*dataNumInSegment);
	for(i=0; i<dataNumInSegment; ++i){
		blockdiskid[i] = blockdiskoffset[i] = 0;
	}

	int Index4LU;
	int Index4U;
	for(i=0; i<fSize; ++i){
		//int fileID = i;
		Index4LU = 0;
		Index4U = 0;
		for(j=0; j<n; ++j){
			Ui[j] = 0;
			GUi[j] = 0;
			LUi[j] = 0;
		}
		for(j=0; j<n; ++j){
			Di[j] = DArray[i*n + j];
		}
		
		for(j=0; j<i; ++j){
			int* LUjtmp = (int*)malloc(sizeof(int)*n);
			for(q=0; q<n; ++q){
				LUjtmp[q] = LU[j*n + q];
			}
			for(q=Index4LU; q<Index4LU+lu[j]; q++){
				if(RArray[q].file_id == i){
					int LUBlockIDInFileJ = q-Index4LU;
					int LUBlockDiskID;
					s = 0;
					for(t=0; t<n; ++t){
						s += LUjtmp[t];
						if(LUBlockIDInFileJ < s){
							LUBlockDiskID = t;
							break;
						}
					}
					Di[LUBlockDiskID]++;
				}
			}
			free(LUjtmp);
			Index4LU += lu[j];
			Index4U += f[j];
		}
		for(j=0; j<n; ++j){
			DArray[i*n + j] = Di[j];
		}
		
		int tmpfi = f[i];
		int block_id = Index4U;
		// first step, place each unique block
		while(tmpfi > 0){
			int tmpDiskID;
			tmpDiskID = CostNextPlacement(r, n, Ui, Di, F, cost_array);
			F[tmpDiskID]++;
			Ui[tmpDiskID]++;
			int tmp = 0;
			int lfid = -1;
			for(q=Index4LU; q<Index4LU+lu[i]; q++){
				if(RArray[q].block_id == block_id){
					tmp = 1;
					if (lfid != RArray[q].file_id) {
						DArray[RArray[q].file_id*n+tmpDiskID]++;
						lfid = RArray[q].file_id;
					}
					//break;
				}
			}
			if(tmp == 0){
				GUi[tmpDiskID]++;
			}
			else{
				LUi[tmpDiskID]++;
			}
			blockdiskid[block_id] = tmpDiskID;
			
			/*Search the row where the offset lies*/
			int row,p_cnt;
			for (row=0;row<(numInSegment/n);row++) {
				if (bitMap[row*n+tmpDiskID] == 0 && groupIden[row*n+tmpDiskID] > 0) {
					blockdiskoffset[block_id] = row;
					edp_solution[block_id].gid = groupIden[row*n+tmpDiskID]-1;
					//fprintf(stderr,"No.%d: GID: %d\n",block_id, edp_solution[block_id].gid);
					p_cnt = 0;
					for (j=0;p_cnt<m;j++){
						if (groupIden[anchors[groupIden[row*n+tmpDiskID]-1]+j] == -(groupIden[row*n+tmpDiskID])) {
							if (p_cnt == 0)
								bitMap[row*n+tmpDiskID] = 1;
							edp_solution[block_id].parities[p_cnt] = (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n;
							p_cnt++;
						}
					}
					break;
				}
			}
			tmpfi--;
			block_id++;
		}
		for(j=0; j<n; ++j){
			U[i*n + j] = Ui[j];
			LU[i*n + j] = LUi[j];
			GU[i*n + j] = GUi[j];
		}
		
		double SI = CostComputeWSDIndex(r, n, i+1, 0, U, DArray, cost_array); // the weighted segment distribution index
		//fprintf(stderr, "segment index: %lf * * * * * * * * * * * * * * * * *\n", SI);
		double fSI = CostComputeWSDIndex(r,n, i+1, i, U, DArray, cost_array);
		//if (fSI < -0.2 )
		CostAdjustPlacement(r, n, DArray, U, GU, i, SI, cost_array);
	}

	Index4U = 0;
	int* newF = (int*)malloc(sizeof(int)*n);
	for(i=0; i<n; ++i){
		newF[i] = 0;
	}
	// TODO
	/* TODO
	for(i=0; i<fSize; ++i){
		for(j=0; j<n; ++j){
			Ui[j] = U[i*n + j];
		}
		for(q=Index4U; q<Index4U + f[i]; ++q){
			//fprintf(stderr, "file id: %d, block id: %d --- ", i, q);
			int blockID = 0;
			int tmpdid, tmpdoffset;
			int breakTag = -1;
			for(t=0; t<n; ++t){
				tmpdid = t;
				tmpdoffset = newF[t];
				for(s=0; s<Ui[t]; ++s){
					if(blockID == q - Index4U){
						breakTag = 1;
						break;
					}
					blockID++;
				}
				if(breakTag == 1){
					break;
				}
			}
			newF[tmpdid]++;
			while(blockIden[tmpdoffset*n + tmpdid] == 1){
				tmpdoffset++;
				newF[tmpdid]++;
			}
			//fprintf(stderr, "location, disk ID: %d, disk offset: %d.\n", tmpdid, tmpdoffset);
			blockdiskid[q] = tmpdid;
			blockdiskoffset[q] = tmpdoffset;
		}
		Index4U += f[i];
	}
	*/
	/*
	for(i=0; i<fSize; ++i){
		for(j=0; j<n; ++j){
			Ui[j] = U[i*n + j];
		}
		int block_id = Index4U;
		int tmpDiskID;
		for(t=0; t<n; ++t){
			tmpDiskID = t;
			for(s=0; s<Ui[t]; ++s){				
				blockdiskid[block_id] = tmpDiskID;
			
				//Search the row where the offset lies
				int row,p_cnt;
				for (row=0;row<(numInSegment/n);row++) {
					if (bitMap[row*n+tmpDiskID] == 0 && groupIden[row*n+tmpDiskID] > 0) {
						blockdiskoffset[block_id] = row;
						edp_solution[block_id].gid = groupIden[row*n+tmpDiskID]-1;
						//fprintf(stderr,"No.%d: GID: %d\n",block_id, edp_solution[block_id].gid);
						p_cnt = 0;
						for (j=0;p_cnt<m;j++){
							if (groupIden[anchors[groupIden[row*n+tmpDiskID]-1]+j] == -(groupIden[row*n+tmpDiskID])) {
										if (p_cnt == 0)
											bitMap[row*n+tmpDiskID] = 1;
										edp_solution[block_id].parities[p_cnt] = (anchors[groupIden[row*n+tmpDiskID]-1]+j)%n;
										p_cnt++;
							}
						}
						break;
					}
				}
				block_id++;
			}
		}
		Index4U += f[i];
	}
	*/
	free(blockIden);
	free(groupIden);
	free(bitMap);

	//fprintf(stderr, "file access time:\n");
	/*
	FILE* fp = fopen("edp_fat.txt", "a+");	
	for(i=0; i<fSize; ++i){
		for(j=0; j<n; ++j){
			Ui[j] = U[i*n + j];
			Di[j] = DArray[i*n + j];
		}
		int maxNumInSingleDisk = -1;
		for(j=0; j<n; ++j){
			if(Ui[j] + Di[j] > maxNumInSingleDisk){
				maxNumInSingleDisk = Ui[j] + Di[j];
			}
		}
		//fprintf(stderr, "file %d, access time %d.\n", i, maxNumInSingleDisk);
		fprintf(fp, "%d %d\n", i, maxNumInSingleDisk);
	}
	fprintf(fp, "%d\n", -1);
	fclose(fp);	
	*/
	//the final block placement
	for(i=0; i<dataNumInSegment; ++i){
		edp_solution[i].node_id = blockdiskid[i];
		edp_solution[i].offset = blockdiskoffset[i];
	}
	free(F);
	//free(pstat);
	free(Ui);
	free(GUi);
	free(LUi);
	free(Di);
	free(lu);
	free(U);
	free(GU);
	free(LU);
	free(newF);
	free(blockdiskid);
	free(blockdiskoffset);
	segment_counter++;
	fprintf(stderr, "===========================SEGMENT %d=============================\n\n", segment_counter);		
	return edp_solution;  
}

/* 
 * Basic Data Placement
 * @param r: number of rows in one segment 
 * @param n: number of disks
 * @param f: number of unique blocks for each file
 * @param fSize: number of files in one segment
 * @param DArray: elements i*n...(i+1)*n records the storage allocation for duplicate blocks in file i
 * @param RArray: records all duplicate blocks and its referred file
 * @param sizeofR: size of RArray
 * @param k: parameter of the adopted erasure code, the number of data
 * @param m: parameter of the adopted erasure code, the number of parity 
 */
struct PlacementInfor* BASLINE(int r, int n, int* f, int fSize, int* DArray, struct ReferIntraSegment* RArray, int sizeofR, int k, int m){
	//fprintf(stderr, "Basic, Basic Data Placement.\n");
	fullcompleteblockdesigntable(k, m, n);
	
	int i,j,q;//,s,t;
	// Store total number of chunks for all files
	int total_chunks = 0;
	// Total number of stripes that are gonna be filled in the segment
	int total_stripes;
	srand (time(NULL));

	//int tupleNum = 1;
	//for(i=n, j=1; j<=(k+m); i--, j++){
	//	tupleNum = tupleNum*i/j;
	//}
	
	//int segment_data_size = tupleNum*(k+m)*(k+m);
	//int* PArray = (int*)malloc(sizeof(int)*segment_data_size);//records the exact positions for each unique block
	
	int* Ui = (int*)malloc(sizeof(int)*n);
	int* GUi = (int*)malloc(sizeof(int)*n);
	int* LUi = (int*)malloc(sizeof(int)*n);
	int* Di = (int*)malloc(sizeof(int)*n);
	
	// compute the number of local unique blocks for each file
	// according to "struct InnerDup* R"
	int* lu = (int*)malloc(sizeof(int)*fSize);
	for(i=0; i<fSize; ++i){
		lu[i] = 0;
		total_chunks += f[i];
	}
	total_stripes =  (total_chunks + k - 1) / k;
	uint8_t* bitmap = (uint8_t*)malloc(sizeof(uint8_t)*r*n/(k+m));
	memset(bitmap,0,sizeof(uint8_t)*r*n/(k+m));

	int* selected_stripes = (int*)malloc(sizeof(int)*total_stripes);
	for (i=0;i<total_stripes;i++){
		int tmp = rand()%(r*n/(k+m));
		while (bitmap[tmp] != 0) tmp = (tmp+1)%(r*n/(k+m));//tmp = rand()%(r*n/(k+m));
		bitmap[tmp] = 1;
		selected_stripes[i]=tmp;
		//fprintf(stderr,"%d, ",tmp);
	}
	//fprintf(stderr,"\n");

	for(i=0; i<sizeofR; ++i){
		int fileID;
		int maxIndex = 0;
		for(j=0; j<fSize; ++j){
			maxIndex += f[j];
			if(RArray[i].block_id < maxIndex){
				fileID = j;
				break;
			}
		}
		lu[fileID]++;
	}
	//fprintf(stderr, "lu, records the number of local unique blocks in each file:\n");
	for(i=0; i<fSize; ++i){
		//fprintf(stderr, "%d ", lu[i]);
	}
	//fprintf(stderr, "\n");
	
	// U, GU, LU
	int* U = (int*)malloc(sizeof(int)*fSize*n);
	int* LU = (int*)malloc(sizeof(int)*fSize*n);
	int* GU = (int*)malloc(sizeof(int)*fSize*n);
	for(i=0; i<fSize*n; ++i){
		U[i] = LU[i] = GU[i] = 0;
	}
	int Index4LU;
	int Index4U;
	int* newF = (int*)malloc(sizeof(int)*n);
	for(i=0; i<n; ++i){
		newF[i] = 0;
	}
	int tmpdid = 0;
	int tmpdoffset;
	int* blockdiskid = (int*)malloc(sizeof(int)*dataNumInSegment);
	int* blockdiskoffset = (int*)malloc(sizeof(int)*dataNumInSegment);
	struct PlacementInfor* basic_solution = (struct PlacementInfor*) malloc (sizeof(struct PlacementInfor)*dataNumInSegment);
	int* stripe = (int*)malloc(sizeof(int)*n);
	memset(stripe,0,sizeof(int)*n);
	for(i=0; i<dataNumInSegment; ++i){
		blockdiskid[i] = blockdiskoffset[i] = 0;
	}
	int cur_chunks = 0;
	struct timeval a,b,c;
	gettimeofday(&a,NULL);
	for(i=0; i<fSize; ++i){
		int tempCounter=0; 
		for(j=0; j<n; j++){
		  tempCounter=tempCounter+DArray[i*n+j];
		}
		//fprintf(stderr, "file id: %d, number of unique(duplicate) blocks: %d(%d).\n", i, f[i], tempCounter);
		//int fileID = i;
		Index4LU = 0;
		Index4U = 0;
		for(j=0; j<n; ++j){
			// note, fi = Ui[0] + Ui[1] + ... + Ui[n-1]
			Ui[j] = 0;
			GUi[j] = 0;
			LUi[j] = 0;
		}
		for(j=0; j<n; ++j){
			Di[j] = DArray[i*n + j];
		}
		
			// Di, should be adjusted considering the upper files in current segment (group)
		for(j=0; j<i; ++j){
			// Rj : <block id, file id>
				// <A1, file1>, <B2, file2>, <C3, file3>, <D4, file4>
			// LUj: (1, 1, 1, 1)
			// GUj; (0, 0, 0, 0)
			//int* LUjtmp = (int*)malloc(sizeof(int)*n);
			//for(q=0; q<n; ++q){
			//	LUjtmp[q] = LU[j*n + q];
			//}
			for(q=Index4LU; q<Index4LU+lu[j]; q++){
				if(RArray[q].file_id == i){
					int block_id = RArray[q].block_id;
					int LUBlockDiskID = blockdiskid[block_id];
					Di[LUBlockDiskID]++;
				}
			}
			//free(LUjtmp);
			Index4LU += lu[j];
			Index4U += f[j];
		}
		for(j=0; j<n; ++j){
			DArray[i*n + j] = Di[j];
		}
		int tmpfi = f[i];
		int block_id = Index4U;
		// place each unique block
		while(tmpfi > 0){
			//fprintf(stderr, "file id: %d, block id: %d --- ", i, block_id);
			/*
			tmpdoffset = newF[tmpdid];
			while(blockIden[tmpdoffset*n + tmpdid] == 1){
				newF[tmpdid]++;
				tmpdid++;
				if(tmpdid == n){
					tmpdid = 0;
				}
				tmpdoffset = newF[tmpdid];
			}
			//fprintf(stderr, "location, disk ID: %d, disk offset: %d.\n", tmpdid, tmpdoffset);
			// TODO
			*/
			/*
			Tmpdid = selected_stripes[cur_chunks/k]*(k+m)%n+cur_chunks%k;
			if (tmpdid >= n) {
				tmpdid = tmpdid % n;
				tmpdoffset = 1 + selected_stripes[cur_chunks/k]*(k+m)/n;
			} else {
				tmpdoffset = selected_stripes[cur_chunks/k]*(k+m)/n;
			}
			*/
			if (cur_chunks%k == 0){
			tmpdid = anchors[selected_stripes[cur_chunks/k]]%n;
			tmpdoffset = anchors[selected_stripes[cur_chunks/k]]/n;
			}
			//fprintf(stderr,"chunk %d to disk %d of start offset %d",cur_chunks,tmpdid,tmpdoffset);
			while(blockIden[tmpdoffset*n + tmpdid] == 1 || stripe[tmpdid] == 1 || groupIden[tmpdoffset*n+tmpdid] != selected_stripes[cur_chunks/k]+1){
				if (stripe[tmpdid] != 1 && groupIden[tmpdoffset*n+tmpdid] == selected_stripes[cur_chunks/k]+1)
					newF[tmpdid]++;

				tmpdid++;
				if(tmpdid == n){
					tmpdid = 0;
					tmpdoffset++;//= newF[tmpdid];
				}
			}
			//fprintf(stderr,"=>final tmpdid %d offset %d\n",tmpdid,tmpdoffset);
			stripe[tmpdid] = 1;
			blockdiskid[block_id] = tmpdid;
			blockdiskoffset[block_id] = tmpdoffset;
			//Search the row where the offset lies
			int p_cnt=0;
			basic_solution[block_id].gid = groupIden[tmpdoffset*n+tmpdid]-1;
			if (cur_chunks%k == 0){
			for (j=0;p_cnt<m;j++){
				if (groupIden[anchors[selected_stripes[cur_chunks/k]]+j] == -(basic_solution[block_id].gid+1)) {
					basic_solution[block_id].parities[p_cnt] = (anchors[selected_stripes[cur_chunks/k]]+j)%n;
					p_cnt++;
				}
			}
			} else {
				for (j=0;j<m;j++) 
					basic_solution[block_id].parities[j] = basic_solution[block_id-1].parities[j];
			}
			cur_chunks++;
			if (cur_chunks % k == 0) memset(stripe,0,sizeof(int)*n);
			// TODO
			newF[tmpdid]++;
			Ui[tmpdid]++;
				// Note, Ui = LUi + GUi
				// check if the current block is the among the LUi
			int tmp = 0;
			for(q=Index4LU; q<Index4LU+lu[i]; q++){
				if(RArray[q].block_id == block_id){
					tmp = 1;
					break;
				}
			}
			if(tmp == 0){
				// GUi
				GUi[tmpdid]++;
			}
			else{
				// LUi
				LUi[tmpdid]++;
			}
			tmpdid++;
			if(tmpdid == n){
				tmpdid = 0;
			}
			tmpfi--;
			block_id++;
		}
		
		for(j=0; j<n; ++j){
			U[i*n + j] = Ui[j];
			LU[i*n + j] = LUi[j];
			GU[i*n + j] = GUi[j];
		}
	}
	gettimeofday(&b,NULL);
	timersub(&b,&a,&c);
	fprintf(stderr,"BASLINE TIME: %ld.%06ld\n",c.tv_sec,c.tv_usec);
	
	/*
	FILE* fp = fopen("basic_fat.txt", "a+");
	for(i=0; i<fSize; ++i){
		for(j=0; j<n; ++j){
			Ui[j] = U[i*n + j];
			Di[j] = DArray[i*n + j];
		}
		int maxNumInSingleDisk = -1;
		for(j=0; j<n; ++j){
			if(Ui[j] + Di[j] > maxNumInSingleDisk){
				maxNumInSingleDisk = Ui[j] + Di[j];
			}
		}
		//fprintf(stderr, "file %d, access time %d.\n", i, maxNumInSingleDisk);
		fprintf(fp, "%d %d\n", i, maxNumInSingleDisk);
	}
	fprintf(fp, "%d\n", -1);
	fclose(fp);	
	*/
	//the final block placement
	for(i=0; i<dataNumInSegment; ++i){
		basic_solution[i].node_id = blockdiskid[i];
		basic_solution[i].offset = blockdiskoffset[i];
	}	
	free(bitMap);
	free(blockIden);
	free(groupIden);
	free(anchors);
	free(newF);
	free(Ui);
	free(LUi);
	free(GUi);
	free(Di);
	free(U);
	free(LU);
	free(GU);
	free(bitmap);
	free(selected_stripes);
	free(stripe);
	free(blockdiskid);
	free(blockdiskoffset);
	segment_counter++;
	//fprintf(stderr, "===========================SEGMENT %d=============================\n\n", segment_counter);	
	return basic_solution;  
}


int NextPlacement2(int n, int* duplicate_array, int* U, int* storage_management){
	int i,j,p;
	int retval;
	j = duplicate_array[n-1]+U[n-1];
	p = storage_management[n-1];
	retval = n-1;
	for(i=n-2; i>=0; --i){
		if(duplicate_array[i]+U[i] < j ){
			j = duplicate_array[i]+U[i];
			p = storage_management[i];
			retval = i;
		}
		else if(duplicate_array[i]+U[i] == j){
			if(storage_management[i] <= p){
				j = duplicate_array[i]+U[i];
				p = storage_management[i];
				retval = i;
			}
		}
	}
	return retval;
}

//
struct PlacementInfor* fEDP(int n, int k, int m, int fid, int unique_num, int* duplicate_array, int* storage_management){
	int i,j;
	struct PlacementInfor* retval = (struct PlacementInfor*)malloc(sizeof(struct PlacementInfor)*unique_num);
	for(i=0; i<unique_num; ++i){
		retval[i].node_id = -100;
		retval[i].offset = -100;
	}
	
	fullcompleteblockdesigntable(k, m, n);
	
	int* U = (int*)malloc(sizeof(int)*n);
	for(i=0; i<n; ++i){
		U[i] = 0;
	}
	
	for(i=0; i<unique_num; ++i){
		int tmpDiskID;
		tmpDiskID = NextPlacement2(n, duplicate_array, U, storage_management);
		U[tmpDiskID]++;
		// this block should be placed in disk indexed by tmpDiskID
		int numInDiskInSegment = numInSegment/n;
		int doffsetInSegment = storage_management[tmpDiskID]%numInDiskInSegment;
		while(blockIden[doffsetInSegment*n + tmpDiskID] == 1){
			  doffsetInSegment++;
			  storage_management[tmpDiskID]++;
		}
		fprintf(stderr, "fid: %d, bid: %d, did:%d, doff:%d.\n", fid, i, tmpDiskID, storage_management[tmpDiskID]);
		retval[i].node_id = tmpDiskID;
		retval[i].offset = storage_management[tmpDiskID];
		storage_management[tmpDiskID]++;
	}
	
	//if((file_dedup_ratio>=(range-1)*0.20)&&(file_dedup_ratio<=range*0.20)){
	  FILE* fp = fopen("edp_fat.txt", "a+");
		  int maxNumInSingleDisk = -1;
		  for(j=0; j<n; ++j){
			  if(U[j] + duplicate_array[j] > maxNumInSingleDisk){
				  maxNumInSingleDisk = U[j] + duplicate_array[j];
			  }
		  }
		  fprintf(stderr, "file access time %d.\n", maxNumInSingleDisk);
		  fprintf(fp, "%d %d\n", fid, maxNumInSingleDisk);
	  fclose(fp);
	//}
	free(U);
	return retval;
}

struct PlacementInfor* fCEDP(int n, int k, int m, int fid, int unique_num, int* duplicate_array, int* cost_array, int* storage_manager){
	// TODO, note: cost here indicates the read throughput of each storage disk, e.g., 12.5 MB/s 
	int i,j;
	struct PlacementInfor* retval = (struct PlacementInfor*)malloc(sizeof(struct PlacementInfor)*unique_num);
	for(i=0; i<unique_num; ++i){
		retval[i].node_id = -100;
		retval[i].offset = -100;
	}
	
	fullcompleteblockdesigntable(k, m, n);
	
	int* U = (int*)malloc(sizeof(int)*n);
	for(i=0; i<n; ++i){
		U[i] = 0;
	}
	
	for(i=0; i<unique_num; ++i){
		int tmpDiskID;
		tmpDiskID = NextPlacement3(n, duplicate_array, U, storage_manager, cost_array);
		U[tmpDiskID]++;
		// this block should be placed in disk indexed by tmpDiskID
		int numInDiskInSegment = numInSegment/n;
		int doffsetInSegment = storage_manager[tmpDiskID]%numInDiskInSegment;
		while(blockIden[doffsetInSegment*n + tmpDiskID] == 1){
			  doffsetInSegment++;
			  storage_manager[tmpDiskID]++;
		}
		fprintf(stderr, "block id: %d, <disk id, disk offset> = <%d, %d>.\n", i, tmpDiskID, storage_manager[tmpDiskID]);
		retval[i].node_id = tmpDiskID;
		retval[i].offset = storage_manager[tmpDiskID];
		storage_manager[tmpDiskID]++;
	}
	
	FILE* fp = fopen("./CostBasedEDP_fileAccessTime.txt", "a+");
		double maxNumInSingleDisk = 0.0;
		for(j=0; j<n; ++j){
			if((double)(U[j] + duplicate_array[j])/cost_array[j] > maxNumInSingleDisk){
				maxNumInSingleDisk = (double)(U[j] + duplicate_array[j])/cost_array[j];
			}
		}
		fprintf(stderr, "file access time %lf.\n", maxNumInSingleDisk);
		fprintf(fp, "%lf\n", maxNumInSingleDisk);
	fclose(fp);
	
	free(U);
	return retval;  
}
struct PlacementInfor* fBASLINE(int n, int k, int m, int fid, int unique_num, int* duplicate_array, int* storage_management){
	int i,j;
	struct PlacementInfor* retval = (struct PlacementInfor*)malloc(sizeof(struct PlacementInfor)*unique_num);
	for(i=0; i<unique_num; ++i){
		retval[i].node_id = -100;
		retval[i].offset = -100;
	}
	
	fullcompleteblockdesigntable(k, m, n);
	
	int* U = (int*)malloc(sizeof(int)*n);
	for(i=0; i<n; ++i){
		U[i] = 0;
	}
	
	int tmpDiskID = n-1;
	int freeoffset = storage_management[n-1];
	for(i=n-2; i>=0; --i){
		if(storage_management[i] <= freeoffset){
			freeoffset = storage_management[i];
			tmpDiskID = i;
		}
	}
	
	for(i=0; i<unique_num; ++i){
		if(i>0){
			tmpDiskID = (tmpDiskID+1)%n;
		}
		int numInDiskInSegment = numInSegment/n;
		int doffsetInSegment = storage_management[tmpDiskID]%numInDiskInSegment;
		while(blockIden[doffsetInSegment*n + tmpDiskID] == 1){
			storage_management[tmpDiskID]++;
			tmpDiskID = (tmpDiskID+1)%n;
			doffsetInSegment = storage_management[tmpDiskID]%numInDiskInSegment;
		}
		retval[i].node_id = tmpDiskID;
		retval[i].offset = storage_management[tmpDiskID];
		storage_management[tmpDiskID]++;
		U[tmpDiskID]++;
	}
	//fprintf(stderr, "fid: %d, fsize:%d.\n", fid, unique_num);
	//if((file_dedup_ratio>=(range-1)*0.20)&&(file_dedup_ratio<=range*0.20)){
	  FILE* fp = fopen("basic_fat.txt", "a+");
		  int maxNumInSingleDisk = -1;
		  //for(j=0; j<n; ++j){
		    //fprintf(stderr, "%d\t", U[j]);
		  //}
		  //fprintf(stderr, "\n");
		  for(j=0; j<n; ++j){
			  if(U[j] + duplicate_array[j] > maxNumInSingleDisk){
				  maxNumInSingleDisk = U[j] + duplicate_array[j];
			  }
		  }
		  //for(j=0; j<n; ++j){
		    //fprintf(stderr, "%d\t", duplicate_array[j]);
		  //}
		  //fprintf(stderr, "\n");		
		  fprintf(stderr, "file access time %d.\n", maxNumInSingleDisk);
		  fprintf(fp, "%d %d\n", fid, maxNumInSingleDisk);
	  fclose(fp);
	//}
	free(U);
	return retval;
}
