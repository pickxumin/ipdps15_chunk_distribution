#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parity_declustering.h"

/*
 * A complete block design, includes all combinations of exactly (k+m)
 * distinct elements selected from the set of N objects.
 * @param k:
 * @param m:
 * @param N:
 * @param index:
 */
int completeblockdesigntable(int k, int m, int N, int index, int* diskOffset, int* blockPlacement, int* blockIdentity, int* groupIden, int* anchors){
	int n = k+m;
	int i,j,p;
	int* tuples;
	int tupleNum = 1;
	int* oneTuple;
	for(i=N, j=1; j<=n; i--, j++){
		tupleNum = tupleNum*i/j;
	}
	//fprintf(stderr, "total tuples' number: %d.\n", tupleNum);
	tuples = (int*)malloc(sizeof(int)*n*tupleNum);
	for(i=0; i<n*tupleNum; ++i){
		tuples[i] = -1;
	}
	oneTuple = (int*)malloc(sizeof(int)*n); // store a tuple
	for(i=0; i<n; ++i){
		oneTuple[i] = -1;
	}
	i=0;
	oneTuple[i] = 0;
	p = -1;
	
	while(i >= 0){
		while(oneTuple[i] < N){
			if(i == n-1){
				p++;
				//fprintf(stderr, "the %d-th tuple:\n", p);
				for(j=0; j<n; ++j){
					tuples[p*n + j] = oneTuple[j];
					//fprintf(stderr, "%d ", oneTuple[j]);
				}
				//fprintf(stderr, "\n");
				
				// TODO, begin	
				int min;
				for(j=0; j<n; ++j){
					//// the j-th block in file p, indexed by p*(k+m) + j
					//blockPlacement[diskOffset[oneTuple[j]]*N + oneTuple[j]] = p*n+j;
					
					//the (index-m+1)-th ... index-th are the parity blocks
					int fileNoTmp = (n-index-1)*tupleNum;
					if (j==0) min = diskOffset[oneTuple[j]]*N + oneTuple[j];
					else {
						if (diskOffset[oneTuple[j]]*N + oneTuple[j] < min) min = diskOffset[oneTuple[j]]*N + oneTuple[j];
					}
					anchors[(-index+m+k-1)*tupleNum+p] = min;
					if(index-m+1>=0){
						if(j < (index-m+1)){
							blockPlacement[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (p+fileNoTmp)*n+j;
							groupIden[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (-index+m+k-1)*tupleNum+(p+1);
						}
						else if((index-m+1) <= j && j <= index){
							blockPlacement[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (p+fileNoTmp)*n+j+n-index-1;
							blockIdentity[diskOffset[oneTuple[j]]*N + oneTuple[j]] = 1;
							groupIden[diskOffset[oneTuple[j]]*N + oneTuple[j]] = -(p+1+(-index+m+k-1)*tupleNum);
						}
						else{
							blockPlacement[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (p+fileNoTmp)*n+j-m;
							groupIden[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (-index+m+k-1)*tupleNum+(p+1);
						}
					}
					else{
						int indTmp = index-m+1+n;
						if(j <= index){
							blockPlacement[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (p+fileNoTmp)*n+j+n-index-1;
							blockIdentity[diskOffset[oneTuple[j]]*N + oneTuple[j]] = 1;
							groupIden[diskOffset[oneTuple[j]]*N + oneTuple[j]] = -((-index+m+k-1)*tupleNum+p+1);
						}
						else if(index < j && j < indTmp){
							blockPlacement[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (p+fileNoTmp)*n+j-index-1; 
							groupIden[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (-index+m+k-1)*tupleNum+1+p;
						}
						else{
							blockPlacement[diskOffset[oneTuple[j]]*N + oneTuple[j]] = (p+fileNoTmp)*n+j-index-1;
							blockIdentity[diskOffset[oneTuple[j]]*N + oneTuple[j]] = 1;
							groupIden[diskOffset[oneTuple[j]]*N + oneTuple[j]] = -((-index+m+k-1)*tupleNum+p+1);
						}
					}
					
					diskOffset[oneTuple[j]]++;
				}
				// TODO, end
				
				oneTuple[i]++;
			}
			else{
				i++;
				oneTuple[i] = oneTuple[i-1] + 1;
			}
		}
		i--;
		if(i>=0){
			oneTuple[i]++;
		}
	}
	
	free(tuples);
	free(oneTuple);
	return 1;
}

/*
 * A full complete block design, includes (k+m) iterations of complete block designs.
 * @param k:
 * @param m:
 * @param N:
 */

int fullcompleteblockdesigntable(int k, int m, int N){
	int i, j;
	int n = k+m;
	int* diskOffset = (int*)malloc(sizeof(int)*N);
	for(i=0; i<N; ++i){
		diskOffset[i] = 0;
	}
	int tupleNum = 1;
	for(i=N, j=1; j<=n; i--, j++){
		tupleNum = tupleNum*i/j;
	}
	int* blockPlacement = (int*)malloc(sizeof(int)*tupleNum*(k+m)*(k+m));
	for(i=0; i<tupleNum*(k+m)*(k+m); ++i){
		blockPlacement[i] = -1;
	}
	int* blockIdentity = (int*)malloc(sizeof(int)*tupleNum*(k+m)*(k+m));
	
	// 0: data block, 1: parity block
	for(i=0; i<tupleNum*(k+m)*(k+m); ++i){
		blockIdentity[i] = 0;
	}
	groupIden = (int*)malloc(sizeof(int)*tupleNum*(k+m)*(k+m));
	bitMap = (int*)malloc(sizeof(int)*tupleNum*(k+m)*(k+m));
	memset(bitMap,0,sizeof(int)*tupleNum*(k+m)*(k+m));
	anchors = (int*)malloc(sizeof(int)*tupleNum*(k+m));
	for(i=k+m-1; i>=0; --i){
		completeblockdesigntable(k, m, N, i, diskOffset, blockPlacement, blockIdentity, groupIden, anchors);
	}
	
	dataNumInSegment = tupleNum*k*(k+m);
	numInSegment = tupleNum*(k+m)*(k+m);
	parityNumInDisk = numInSegment/N - dataNumInSegment/N;
	//fprintf(stderr, "dataNumInSegment: %d, numInSegment: %d, parityNumInDisk: %d\n", dataNumInSegment, numInSegment, parityNumInDisk);
	blockIden = (int*)malloc(sizeof(int)*tupleNum*(k+m)*(k+m));
	for(i=0; i<tupleNum*(k+m)*(k+m); ++i){
		blockIden[i] = blockIdentity[i];
	}
	/*
	fprintf(stderr, "groupIdentity:\n");
	for(i=0; i<tupleNum*(k+m)*(k+m); ++i){
		fprintf(stderr, "%d ", groupIden[i]);
		if(i%N == N-1){
			fprintf(stderr, "\n");
 		}
 		if(i%(tupleNum*(k+m)) == tupleNum*(k+m)-1 && i!= tupleNum*(k+m)*(k+m)-1){
			fprintf(stderr, "-----------------------------------------------\n");
		}
	}
	fprintf(stderr, "groupIdentity.\n");
	*/
	/*	
	// some output information !!!
	for(i=0; i<tupleNum*(k+m)*(k+m); ++i){
		if(blockIdentity[i] == 0){
		fprintf(stderr, "D(%d,%d) ", blockPlacement[i]/n, blockPlacement[i]%n);
		}
		else{
			fprintf(stderr, "P(%d,%d) ", blockPlacement[i]/n, blockPlacement[i]%n);
		}
		if(i%N == N-1){
			fprintf(stderr, "\n");
 		}
 		if(i%(tupleNum*(k+m)) == tupleNum*(k+m)-1 && i!= tupleNum*(k+m)*(k+m)-1){
			fprintf(stderr, "-----------------------------------------------\n");
		}
	}
	*/
	//TODO, user oriented
	int* dataDiskIDs;
	int* dataDiskOffsets;
	int* parityDiskIDs;
	int* parityDiskOffsets;
	dataDiskIDs = (int*)malloc(sizeof(int)*tupleNum*k*(k+m));
	dataDiskOffsets = (int*)malloc(sizeof(int)*tupleNum*k*(k+m));
	parityDiskIDs = (int*)malloc(sizeof(int)*tupleNum*m*(k+m));
	parityDiskOffsets = (int*)malloc(sizeof(int)*tupleNum*m*(k+m));
	for(i=0; i<tupleNum*k*(k+m); ++i){
		dataDiskIDs[i] = dataDiskOffsets[i] = -1;
	}
	for(i=0; i<tupleNum*m*(k+m); ++i){
		parityDiskIDs[i] = parityDiskOffsets[i] = -1;
	}
	int dataIndex = -1;
	int parityIndex = -1;
	for(i=0; i<tupleNum*(k+m)*(k+m); ++i){
		int dID = i%N;
		int dOffset = i/N;
		if(blockIdentity[i] == 0){
			dataIndex = blockPlacement[i]/n*k + blockPlacement[i]%n;
			dataDiskIDs[dataIndex] = dID;
 			dataDiskOffsets[dataIndex] = dOffset;
		}
		else{
			parityIndex = blockPlacement[i]/n*m + blockPlacement[i]%n - k;
			parityDiskIDs[parityIndex] = dID;
			parityDiskOffsets[parityIndex] = dOffset;
		}
		// the dataIndex-th data block, or the parityIndex-th parity block
		
	}
	/*
	for(i=0; i<tupleNum*k*(k+m); ++i){
		fprintf(stderr, "the %d-th data block's location: (%d, %d).\n", i, dataDiskIDs[i], dataDiskOffsets[i]);
	}
	for(i=0; i<tupleNum*m*(k+m); ++i){
		fprintf(stderr, "the %d-th parity block's location: (%d, %d).\n", i, parityDiskIDs[i], parityDiskOffsets[i]);
	}
	*/
	/*
	int parityNum = Num/k*m;
	int groupID, groupOffset;
	int groupHeight = tupleNum*(k+m)*(k+m)/N;
	for(i=0; i<Num; ++i){
		groupID = i/(tupleNum*k*(k+m));
		groupOffset = i%(tupleNum*k*(k+m));
		fprintf(stderr, "the %d-th data block's location: (%d, %d).\n", i, dataDiskIDs[groupOffset], dataDiskOffsets[groupOffset]+groupID*groupHeight);
	}
	for(i=0; i<parityNum; ++i){
		groupID = i/(tupleNum*m*(k+m));
		groupOffset = i%(tupleNum*m*(k+m));
		fprintf(stderr, "the %d-th parity block's location: (%d, %d).\n", i, parityDiskIDs[groupOffset], parityDiskOffsets[groupOffset]+groupID*groupHeight);
	}
	*/
	
	free(diskOffset);
	free(blockPlacement);
	free(blockIdentity);
	free(dataDiskIDs);
	free(dataDiskOffsets);
	free(parityDiskIDs);
	free(parityDiskOffsets);
	return 1;
}

/*
int main(int argc, char* argv[]){
	int retstat = 1;
	if(argc != 5){
		fprintf(stderr, "Usage: ./paritydeclustering (k) (m) (N) (dataBlockNum).\n");
		exit(-1);
	}
	int k, m, N, Num;
	k = atoi(argv[1]);
	m = atoi(argv[2]);
	N = atoi(argv[3]); // number of disks
	Num = atoi(argv[4]); // number of user blocks
	
	fullcompleteblockdesigntable(k, m, N, Num);
	
	return retstat;
}*/

// data flow, block ids: 0, 1, ..., 59||, 0, 1, ..., 59||, 0, 1, ..., 59
// data flow, file ids: 0, 1, 2, 3, 4||, 0, 1, 2, 3||, 0, 1, 2,
