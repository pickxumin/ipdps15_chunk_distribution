#ifndef _DEDUP_HH_
#define _DEDUP_HH_

/*
 * A structure recording how duplicate blocks are referred by other files in one segment 
 */
struct ReferIntraSegment{
      int block_id; //block id
      int file_id; //id of the file that contains this duplicate block
	  int ab_file_id;
};

/*
 * A structure recording how duplicate blocks are referred by other files in one segment 
 */
struct PlacementInfor{
      uint8_t node_id; //storage node id
      int offset; //in the unit of block
	  uint64_t gid; //ID of encoding group
	  uint8_t parities[4]; //array of parity disk IDs
};

/*
 * A structure recording the storage allocation information for each file
 */
struct FileInfo {
	int fileId; //file id
	double fdi; //file fairness distribution index
	int n_unique; //number of unique blocks in the file
	int n_duplication; //number of duplicate blocks in the file 
	int* n_uniqueBlocks; //storage allocation for unique blocks, variable
	int* n_duplicationBlocks; //storage allocation for duplicate blocks, determined
};

extern struct PlacementInfor* CEDP(int r, int n, int* f, int fSize, int* DArray, struct ReferIntraSegment* RArray, int* cost_array, int sizeofR, int k, int m);
extern struct PlacementInfor* EDP(int r, int n, int* f, int fSize, int* DArray, int* PDArray, struct ReferIntraSegment* RArray, int sizeofR, int k, int m);
extern struct PlacementInfor* BASLINE(int r, int n, int* f, int fSize, int* DArray, struct ReferIntraSegment* RArray, int sizeofR, int k, int m);

extern struct PlacementInfor* fCEDP(int n, int k, int m, int fid, int unique_num, int* duplicate_array, int* cost_array, int* storage_manager);
extern struct PlacementInfor* fEDP(int n, int k, int m, int fid, int unique_num, int* duplicate_array, int* storage_manager);
extern struct PlacementInfor* fBASLINE(int n, int k, int m, int fid, int unique_num, int* duplicate_array, int* storage_manager);
#endif
