/**
 * @file 	fsl_analytic.c
 * @brief	Program that generates metadata file for further processing
 * @author	Xu Min
 */
#include <inttypes.h> 
#include <sys/types.h>
#include <kclangc.h>
#include <openssl/sha.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#define MAXLINE	4096
#define FP_SIZE 20
#define MAX_ENTRIES(x) ((1024ULL<<21) * x)
#define MMAP_FD(fd, size) mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)

typedef struct vm_stat {
	uint64_t total_logical;
	uint64_t total_physical;
} VM_STAT;


int main(int argc, char** argv){
	
	if (argc != 2){
		printf("\nUsage:\n%s <snapshot file>\n",argv[0]);
		return 0;
	}
	uint64_t new_logical=0, new_physical=0;
	char path1[64];
	//char path2[64];
	//Load Statistics
	sprintf(path1,"meta/4430log2");
	int fd = open(path1,O_RDWR|O_CREAT,0644);
	assert(!ftruncate(fd,sizeof(VM_STAT)));
	VM_STAT* vm_stat = MMAP_FD(fd,sizeof(VM_STAT));
	close(fd);

	//Load Index File
	KCDB* db = kcdbnew();
	kcdbopen(db,"meta/4430index.kch#bnum=400M#msiz=2G",KCOWRITER|KCOCREATE);
	//sprintf(path1,"meta/4430index");
	//kcdbloadsnap(db,path1);

	/*
	KCDB* dailydb = kcdbnew();
	kcdbopen(dailydb,"-",KCOWRITER|KCOCREATE);
	sprintf(path2,"meta/fslindex_%s",argv[3]);
	kcdbloadsnap(dailydb,path2);
	*/

	//Chunking the FSL Trace
	int ifd = open(argv[1],O_RDONLY);
	uint64_t offset = 0, flag=1;
	uint64_t size, i;
	uint8_t * fps = malloc(FP_SIZE);
    while ((size = read(ifd, fps, FP_SIZE)) > 0) {
		void* dptr = kcdbget(db,(const char*)fps, FP_SIZE, &size);
		if (dptr == NULL) {
			kcdbadd(db,(const char*)fps, FP_SIZE, (const char*)&flag, sizeof(uint64_t));
			vm_stat->total_logical += 4096ULL;
			vm_stat->total_physical += 4096ULL;
			new_logical += 4096ULL;
			new_physical += 4096ULL;
		} else {
			vm_stat->total_logical += 4096ULL;
			new_logical += 4096ULL;
		}
	}

	free(fps);
	close(ifd);

	fprintf(stderr,"Total Logical: %ld\nTotal Physical: %ld\nNew Logical: %ld\nNew Physical: %ld\n",vm_stat->total_logical,vm_stat->total_physical,new_logical,new_physical);

	//Release unused memory
	//kcdbdumpsnap(db,path1);
	kcdbsync(db,1,NULL,NULL);
	kcdbclose(db);
	kcdbdel(db);
	/*
	kcdbdumpsnap(dailydb,path2);
	kcdbclose(dailydb);
	kcdbdel(dailydb);
	*/
	munmap(vm_stat,sizeof(VM_STAT));
	return 0;
}
