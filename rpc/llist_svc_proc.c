#include "llist.h"
#include "../common.h"
#include <kclangc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <endian.h>
#include <assert.h>

Size is;
KCDB* map;
KCDB* csmap;
struct stat sdstat;
struct stat csstat;

Size * fetch_size_1_svc(image* img, struct svc_req *req){
	int fd = open("../meta/ilog",O_RDONLY);
	assert(fd != -1);
	IMEntry* en = MMAP_FD_RO(fd,INST_MAX(sizeof(IMEntry)));
	close(fd);
	uint64_t size = en[img->ins].vers[img->vers].size;

	char buf[64];
	sprintf(buf, "../image/%ld/%ld-%d",img->ins/256,img->ins,img->vers);
	fd = open(buf,O_RDONLY);
	printf("Image: %s\n",buf);
	assert(fd != -1);
	int _size = lseek(fd,0,SEEK_END);
	close(fd);
	int cnt = _size/sizeof(Direct);
	munmap(en,INST_MAX(sizeof(IMEntry)));

	is.bytes = size;
	is.chks = cnt;

	return &is;
}

list * fetch_list_1_svc(path* pth, struct svc_req *req){
	char buf[64];
	sprintf(buf, "../image/%s",pth->name);
	int fd = open(buf,O_RDONLY);
	printf("Image: %s\n",buf);
	uint32_t start = pth->start;
	uint32_t end = pth->end;
	assert(fd != -1);
	struct stat sdtmp;
	/*
	if (map == NULL){
		printf("load sdmap.kch\n");
		map = kcdbnew();
		kcdbopen(map,"../meta/sdmap.kch#bnum=100M#msiz=1G",KCOWRITER|KCOCREATE|KCONOLOCK);
		//kcdbloadsnap(map,"../meta/sdmap.kch");
	}
	//check whether sdmap is obsolete
    	if (stat("../meta/sdmap.kch",&sdtmp) == -1) {
		perror("../meta/sdmap.kch");
		exit(1);
	}
	if (sdtmp.st_mtime != sdstat.st_mtime) {
	*/
	if (map != NULL ){
		kcdbclose(map);
		kcdbdel(map);
	}
		map = kcdbnew();
		kcdbopen(map,"../meta/sdmap.kch#bnum=100M#msiz=1G",KCOREADER|KCONOLOCK);
	/*
		//kcdbloadsnap(map,"../meta/sdmap.kch");
		sdstat.st_mtime = sdtmp.st_mtime;
	}
	
	if (csmap == NULL){
		printf("load csmap.kch\n");
		csmap = kcdbnew();
		kcdbopen(csmap,"../meta/csmap.kch#bnum=100M#msiz=1G",KCOWRITER|KCOCREATE|KCONOLOCK);
		//kcdbloadsnap(csmap,"../meta/csmap.kch");
	}
	//check whether sdmap is obsolete
    	if (stat("../meta/csmap.kch",&sdtmp) == -1) {
		perror("../meta/csmap.kch");
		exit(1);
	}
	if (sdtmp.st_mtime != csstat.st_mtime) {
	*/
	if (csmap != NULL) {
		//kcdbloadsnap(csmap,"../meta/csmap.kch");
		kcdbclose(csmap);
		kcdbdel(csmap);
	}
		csmap = kcdbnew();
        kcdbopen(csmap,"../meta/csmap.kch#bnum=100M#msiz=1G",KCOREADER|KCONOLOCK);
	/*
		csstat.st_mtime = sdtmp.st_mtime;
	}
	*/

	int size = lseek(fd,0,SEEK_END);
	Direct* dir = (Direct*)MMAP_FD_RO(fd,size);
	close(fd);

	//Load Stripe Log
	fd = open("../meta/stlog",O_RDONLY);
	STEntry* sten = MMAP_FD_RO(fd,MAX_ENTRIES(sizeof(STEntry))/STRIPE_SIZE);
	if (sten == (void*)-1){
		perror("MMAP STLOG");
		exit(1);
	}	
	close(fd);

	int cnt = size/sizeof(Direct);
	int i,j;
	static list result;
	list* tmp,*tail;
	uint64_t _size;
	void* dptr,*sptr;

	// Free previous result
	xdr_free(xdr_list,&result);

	//Fetch direct entries, and
	//pack them into a list
	printf("Number of segments %d, %d, %d\n",cnt, start, end);
	for(i=start;i<end;i++){
		//printf("No. %d item\n",i);
		if(i==start){
			tail = tmp = (list*)malloc(sizeof(list));
			assert(tmp != NULL);
			tmp->id = dir[i].id;
			tmp->offset = dir[i].index;
			dptr = kcdbget(map, (char*)&tmp->id,sizeof(uint64_t),&_size);
			if(dptr != NULL)
	            		tmp->disk = *(uint8_t*)dptr;
			else
				fprintf(stderr,"chunk not found.\n");
				//tmp->disk = dir[i].disk;
			tmp->len = dir[i].len;
			tmp->next = NULL;
			//tmp->stid = dir[i].stid;
			sptr = kcdbget(csmap,(char*)&tmp->id,sizeof(uint64_t),&_size);
			if (sptr != NULL)
				tmp->stid = *(uint64_t*)sptr;
			else
				fprintf(stderr,"stripe not found.\n");
			tmp->stlen = sten[tmp->stid].len;
			//tmp->st_sid = (uint64_t*)malloc(sizeof(uint64_t)*STRIPE_SIZE);
			//printf("%ld",tmp->stid);
			for (j=0;j<sten[tmp->stid].len;j++){
				tmp->st_chk[j].id = sten[tmp->stid].sid[j];
				tmp->st_chk[j].len = sten[tmp->stid].size[j];
				tmp->st_chk[j].disk = sten[tmp->stid].ddisk[j];
				//printf("%ld(%d),",tmp->st_chk[j].disk,tmp->st_chk[j].len);
			}
			for (j=sten[tmp->stid].len;j<STRIPE_SIZE-CODE_SIZE;j++){
				tmp->st_chk[j].id = 0;
				//printf("%ld,",tmp->st_chk[j].id);
			}
			for (j=0;j<CODE_SIZE;j++) {
				tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].id = sten[tmp->stid].sid[j+STRIPE_SIZE-CODE_SIZE];
				tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].len = sten[tmp->stid].size[j+STRIPE_SIZE-CODE_SIZE];
				tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].disk = sten[tmp->stid].ddisk[j+STRIPE_SIZE-CODE_SIZE];
				//printf("%ld(%d),",tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].disk,tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].len);
			}
			//printf("]\n");
			//result = tmp;
			memcpy(&result,tmp,sizeof(list));
			tail = &result;
			free(tmp);
		} else {
			tmp = (list*)malloc(sizeof(list));
			assert(tmp != NULL);
			tmp->id = dir[i].id;
			tmp->offset = dir[i].index;
			
			dptr = kcdbget(map, (char*)&tmp->id,sizeof(uint64_t),&_size);
			if(dptr != NULL)
	            tmp->disk = *(uint8_t*)dptr;
			else
				fprintf(stderr,"chunk not found.\n");
				//tmp->disk = dir[i].disk;
			tmp->len = dir[i].len;
			//tmp->stid = dir[i].stid;
			sptr = kcdbget(csmap,(char*)&tmp->id,sizeof(uint64_t),&_size);
			if (sptr != NULL)
				tmp->stid = *(uint64_t*)sptr;
			else
				fprintf(stderr,"stripe not found.\n");
			tmp->stlen = sten[tmp->stid].len;
			//tmp->st_sid = (uint64_t*)malloc(sizeof(uint64_t)*STRIPE_SIZE);
			//printf("%ld\n",tmp->stid);
			for (j=0;j<sten[tmp->stid].len;j++){
				tmp->st_chk[j].id = sten[tmp->stid].sid[j];
				tmp->st_chk[j].len = sten[tmp->stid].size[j];
				tmp->st_chk[j].disk = sten[tmp->stid].ddisk[j];
				//printf("%ld(%d),",tmp->st_chk[j].disk,tmp->st_chk[j].len);
			}
			for (j=sten[tmp->stid].len;j<STRIPE_SIZE-CODE_SIZE;j++){
				tmp->st_chk[j].id = 0;
				//printf("%ld,",tmp->st_chk[j].id);
			}
			for (j=0;j<CODE_SIZE;j++) {
				tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].id = sten[tmp->stid].sid[j+STRIPE_SIZE-CODE_SIZE];
				tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].len = sten[tmp->stid].size[j+STRIPE_SIZE-CODE_SIZE];
				tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].disk = sten[tmp->stid].ddisk[j+STRIPE_SIZE-CODE_SIZE];
				//printf("%ld(%d),",tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].disk,tmp->st_chk[j+STRIPE_SIZE-CODE_SIZE].len);
			}
			//printf("]\n");

			tmp->next = NULL;
			tail->next = tmp;
			tail = tmp;
		} 
	}

	//kcdbsync(map,1,NULL,NULL);
	//kcdbsync(csmap,1,NULL,NULL);
	//kcdbdumpsnap(map, "../meta/sdmap");
	//kcdbclose(map);
	//kcdbdel(map);
	munmap(dir,size);
	munmap(sten,MAX_ENTRIES(sizeof(STEntry))/STRIPE_SIZE);
	return (&result);
}
