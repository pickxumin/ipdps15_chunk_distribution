struct Chunk {
	uint64_t id;
	uint8_t disk;
	uint32_t len;
};

struct list {
	uint64_t offset;
	uint64_t id;
	uint8_t disk;
	uint32_t len;
	uint64_t stid;
	uint8_t stlen;
	Chunk st_chk[14];
	list* next;
};

struct image {
	uint64_t ins;
	uint8_t vers; 
};

struct path {
	string name<>;
	uint32_t start;
	uint32_t end;
};

struct Size {
	uint64_t bytes;
	uint64_t chks;
};

program FETCHER {
	version FETCHER_V1 {
		list FETCH_LIST(path) = 1;
		Size FETCH_SIZE(image) = 2;
	} = 1;
} = 0x2fffffff;
