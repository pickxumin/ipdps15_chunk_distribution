#include "llist.h"

int main(int argc, char* argv[]){
	
	if(argc != 5){
		printf("Usage: %s hostname filename start end\n",argv[0]);
		exit(1);
	}

	list* result;
	uint64_t* size;
	path* pth;
	image* img;
	Size* is;

	CLIENT *cl = clnt_create(argv[1],FETCHER, FETCHER_V1, "tcp");
	if(cl == NULL){
		printf("error: could not connect to server.\n");
		return 1;
	}

	pth = (path*)malloc(sizeof(path));
	pth->name = argv[2];
	pth->start = atoi(argv[3]);
	pth->end = atoi(argv[4]);
	img = (image*)malloc(sizeof(image));
	img->ins = 0;
	img->vers = 0;
	result = fetch_list_1(pth,cl);
	is = fetch_size_1(img,cl);
	if (result == NULL) {
	    printf("error: RPC failed!\n");
	    return 1;
	}
	
	printf("Size of file: %ld, %ld chunks\n",is->bytes, is->chks);
	list* ptr;
	ptr = result;
	while(ptr != NULL){
		printf("Segment %ld on disk %d, length %d\n",ptr->id,ptr->disk,ptr->len);
		ptr = ptr->next;
	}
	free(pth);	
	free(img);
	return 0;
}
