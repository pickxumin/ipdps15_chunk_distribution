#include "llist.h"
#include "../common.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <endian.h>
#include <assert.h>


int main(int argc, char* argv[]){

	int fd = open("../image/1-0",O_RDONLY);
	assert(fd != -1);
	
	int size = lseek(fd,0,SEEK_END);
	Direct* dir = (Direct*)MMAP_FD_RO(fd,size);
	close(fd);
	int cnt = size/sizeof(Direct);
	int i;

	//Fetch direct entries, and
	//pack them into a list
	for(i=0;i<cnt;i++){
		printf("Segment %ld on disk %d at offset %ld\n",dir[i].id, dir[i].disk, dir[i].index);		
	}

	munmap(dir,size);

	return 0;
}
